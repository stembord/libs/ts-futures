import { none, Option, some } from "@stembord/core/src/option";

export type IStreamResolve<T> = (value: Option<T>) => void;
export type IStreamResolver<T> = (resolve: IStreamResolve<T>) => void;

export type IStreamCallback<T, U> = (value: T) => U;
export type IStreamHandler<T> = (value: Option<T>) => void;

export type IExtractStreamGeneric<Type> = Type extends Stream<infer X>
  ? X
  : Type extends Future<infer X>
  ? X
  : Type;

export enum StreamState {
  pending = "pending",
  resolved = "resolved"
}

const defaultGetKey = (value: any) => value;
const defaultGetValue = (value: any) => value;

export class Stream<T> {
  static from<T>(...values: T[]): Stream<T> {
    return new Stream(resolve => {
      values.forEach(value => resolve(some(value)));
      resolve(none());
    });
  }

  static isStream<T>(value: any): value is Stream<T> {
    return value instanceof Stream;
  }

  static all<T>(...streams: Array<Stream<T>>): Stream<T> {
    return new Stream(resolve => {
      streams.forEach(stream => {
        stream.then(values => {
          values.forEach(value => resolve(some(value)));
        });
      });
    });
  }

  static race<T>(...streams: Array<Stream<T>>): Stream<T> {
    return new Stream(resolve => {
      let resolved = false;

      streams.forEach(stream => {
        stream.then(values => {
          if (resolved) {
            stream.cancel();
          } else {
            resolved = true;
            values.forEach(value => resolve(some(value)));
            resolve(none());
          }
        });
      });
    });
  }
  private from: Option<Stream<any>> = none();
  private state: StreamState = StreamState.pending;
  private canceled: boolean = false;
  private values: Array<Option<T>> = [];
  private handlers: Array<IStreamHandler<T>> = [];

  constructor(resolver: IStreamResolver<T>) {
    this.resolve(resolver);
  }

  getState() {
    return this.state;
  }

  cancel() {
    if (!this.canceled) {
      this.canceled = true;
      this.from.map(from => from.cancel());
    }
    return this;
  }

  pause() {
    return this.cancel();
  }

  resume() {
    if (this.canceled) {
      this.canceled = false;

      if (this.from.isSome()) {
        this.from.unwrap().resume();
      } else if (this.state === StreamState.pending) {
        this.resolveHandlers();
      }
    }
    return this;
  }

  map<U = T>(
    callback: IStreamCallback<T, U>
  ): Stream<IExtractStreamGeneric<U>> {
    const stream: Stream<IExtractStreamGeneric<U>> = new Stream(resolve => {
      this.handle(value => {
        if (value.isSome()) {
          resolve(some(callback(value.unwrap()) as IExtractStreamGeneric<U>));
        } else {
          resolve(none());
        }
      });
    });
    stream.from = some(this);
    return stream;
  }

  take(size: number): Stream<T> {
    size = size <= 0 ? 1 : size;

    const stream: Stream<T> = new Stream(resolve => {
      this.handle(value => {
        if (value.isSome()) {
          resolve(value);
        } else {
          resolve(none());
          return;
        }

        if (--size === 0) {
          resolve(none());
        }
      });
    });
    stream.from = some(this);
    return stream;
  }

  reduce<C>(acc: C, fn: (acc: C, value: T) => C): Future<C> {
    return new Future(resolve => {
      this.handle(option => {
        if (option.isSome()) {
          const value = option.unwrap();
          acc = fn(acc, value);
        } else {
          resolve(acc);
        }
      });
    });
  }

  toArray(): Future<T[]> {
    return this.reduce<T[]>([], (array, value) => {
      array.push(value);
      return array;
    });
  }

  toMap<K extends keyof any, V>(
    getKey: (value: T) => K = defaultGetKey,
    getValue: (value: T) => V = defaultGetValue
  ): Future<Record<K, V>> {
    return this.reduce<Record<K, V>>({} as any, (object, value) => {
      object[getKey(value)] = getValue(value);
      return object;
    });
  }

  then<U = T>(
    callback: IFutureCallback<T[], U>
  ): Future<IExtractFutureGeneric<U>> {
    return this.toArray().then(callback);
  }

  filter(fn: (value: T) => boolean): Stream<T> {
    const stream: Stream<T> = new Stream(resolve => {
      this.handle(next => {
        if (next.isSome()) {
          if (fn(next.unwrap())) {
            resolve(next);
          }
        } else {
          resolve(none());
        }
      });
    });
    stream.from = some(this);
    return stream;
  }

  forEach(fn: (value: T) => void): Stream<T> {
    const stream: Stream<T> = new Stream(resolve => {
      this.handle(value => {
        value.map(fn);
        resolve(value);
      });
    });
    stream.from = some(this);
    return stream;
  }

  private resolve(resolver: IStreamResolver<T>) {
    resolver(this.resolveValue);
  }

  private resolveValue = (option: Option<T>) => {
    if (option.isSome()) {
      const value = option.unwrap();

      if ((value as any) === this) {
        throw new TypeError("A Stream cannot be resolved with itself");
      } else if (Stream.isStream<T>(value)) {
        value.then(values => {
          values.forEach(value => this.values.push(some(value)));
          this.resolveHandlers();
        });
      } else if (Future.isFuture<T>(value)) {
        value.then(value => {
          this.values.push(some(value));
          this.resolveHandlers();
        });
      } else {
        this.values.push(option);
        this.resolveHandlers();
      }
    } else {
      this.values.push(option);
      this.resolveHandlers();
    }
  };

  private resolveHandlers() {
    process.nextTick(() => {
      if (!this.canceled) {
        this.handlers.forEach(this.resolveHandler);
      }
    });
  }

  private resolveHandler = (handler: IStreamHandler<T>) => {
    const values = this.values.slice();
    this.values.length = 0;

    if (this.state === StreamState.pending) {
      values.forEach(handler);
    }
  };

  private handle(handler: IStreamHandler<T>) {
    this.handlers.push(handler);

    process.nextTick(() => {
      if (this.state === StreamState.pending) {
        this.resolveHandler(handler);
      }
    });
  }
}

import { throws } from "assert";
import { Future, IExtractFutureGeneric, IFutureCallback } from "./Future";
