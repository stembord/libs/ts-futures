import { none, Option, some } from "@stembord/core/src/option";
import { isFunction, isObject } from "util";

export type IFutureResolve<T> = (value: T) => void;
export type IFutureResolver<T> = (resolve: IFutureResolve<T>) => void;

export type IFutureCallback<T, U> = (value: T) => U;
export type IFutureHandler<T> = (value: T) => void;

export type IExtractFutureGeneric<Type> = Type extends Future<infer X>
  ? X
  : Type extends Promise<infer X>
  ? X
  : Type;

export enum FutureState {
  pending = "pending",
  resolved = "resolved"
}

export class Future<T> {
  static from<T>(value: T): Future<T> {
    return new Future(resolve => resolve(value));
  }

  static isFuture<T>(value: any): value is Future<T> {
    return (
      value && (isObject(value) || isFunction(value)) && isFunction(value.then)
    );
  }

  static all<T>(...futures: Array<Future<T>>): Future<T[]> {
    return new Future(resolve => {
      let count = futures.length;
      const results = new Array(count);

      futures.forEach((future, index) => {
        return future.then(value => {
          results[index] = value;

          if (--count === 0) {
            resolve(results);
          }
        });
      });
    });
  }

  static race<T>(...futures: Array<Future<T>>): Future<T> {
    return new Future(resolve => {
      let resolved = false;

      futures.forEach(future => {
        future.then(value => {
          if (!resolved) {
            resolved = true;
            resolve(value);
          }
        });
      });
    });
  }
  private from: Option<Future<any>> = none();
  private canceled: boolean = false;
  private value?: T;
  private state: FutureState = FutureState.pending;
  private handlers: Array<IFutureHandler<T>> = [];

  constructor(resolver: IFutureResolver<T>) {
    this.resolve(resolver);
  }

  getState(): FutureState {
    return this.state;
  }

  cancel(): Future<T> {
    this.canceled = true;
    this.from.map(from => from.cancel());
    return this;
  }

  pause(): Future<T> {
    return this.cancel();
  }

  resume(): Future<T> {
    this.canceled = false;

    if (this.from.isSome()) {
      this.from.unwrap().resume();
    } else if (this.state === FutureState.resolved) {
      this.resolveHandlers();
    }

    return this;
  }

  then<U = T>(
    callback: IFutureCallback<T, U>
  ): Future<IExtractFutureGeneric<U>> {
    const future: Future<IExtractFutureGeneric<U>> = new Future(resolve => {
      this.handle(value => {
        resolve(callback(value) as IExtractFutureGeneric<U>);
      });
    });
    future.from = some(this);
    return future;
  }

  map<U = T>(
    callback: IFutureCallback<T, U>
  ): Future<IExtractFutureGeneric<U>> {
    return this.then(callback);
  }

  private resolve(resolver: IFutureResolver<T>) {
    resolver(value => this.resolveValue(value));
  }

  private resolveValue(value: T) {
    if ((value as any) === this) {
      throw new TypeError("A Future cannot be resolved with itself");
    } else {
      if (Future.isFuture<T>(value)) {
        this.resolve(resolve => {
          value.then(resolve);
        });
      } else {
        this.state = FutureState.resolved;
        this.value = value;
        this.resolveHandlers();
      }
    }
  }

  private resolveHandlers() {
    if (!this.canceled) {
      const handlers = this.handlers.slice();
      this.handlers.length = 0;
      handlers.forEach(handler => this.handle(handler));
    }
  }

  private handle(handler: IFutureHandler<T>) {
    if (this.state === FutureState.pending) {
      this.handlers.push(handler);
    } else if (!this.canceled) {
      process.nextTick(() => {
        handler(this.value as T);
      });
    }
  }
}
