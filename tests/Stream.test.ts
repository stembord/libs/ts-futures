import { none, some } from "@stembord/core";
import * as tape from "tape";
import { Future, Stream } from "../lib";

tape("Stream custom", (assert: tape.Test) => {
  new Stream<number>(onNext =>
    [some(1), some(2), some(3), some(4)].forEach(onNext)
  )
    .map(value => value * 3)
    .filter(value => value % 2 === 0)
    .map(value => Stream.from(value))
    .map(value => Future.from(value))
    .take(2)
    .then(results => {
      assert.deepEqual(results, [6, 12]);
      assert.end();
    });
});

tape("Stream from", (assert: tape.Test) => {
  Stream.from(1, 2, 3, 4)
    .map(value => value * 3)
    .filter(value => value % 2 === 0)
    .then(results => {
      assert.deepEqual(results, [6, 12]);
      assert.end();
    });
});

tape("Stream cancel", (assert: tape.Test) => {
  let called = false;

  new Stream<number>(onNext =>
    [some(1), some(2), some(3), some(4), none<number>()].forEach(value =>
      setTimeout(() => onNext(value), 1)
    )
  )
    .map(value => value * 3)
    .filter(value => value % 2 === 0)
    .cancel()
    .then(() => {
      called = true;
    });

  setTimeout(() => {
    assert.deepEqual(called, false);
    assert.end();
  }, 5);
});

tape("Stream all", (assert: tape.Test) => {
  Stream.all(Stream.from(0), Stream.from(1), Stream.from(2))
    .take(3)
    .then(results => {
      assert.deepEqual(results, [0, 1, 2]);
      assert.end();
    });
});

tape("Stream race", (assert: tape.Test) => {
  Stream.race(Stream.from(0), Stream.from(1), Stream.from(2)).then(result => {
    assert.deepEqual(result, [0]);
    assert.end();
  });
});

tape("Stream async", async (assert: tape.Test) => {
  const result = await Stream.from(0, 1, 2, 3);
  assert.deepEqual(result, [0, 1, 2, 3]);
  assert.end();
});
