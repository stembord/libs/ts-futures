import * as tape from "tape";
import { Future } from "../lib";

tape("Future", (assert: tape.Test) => {
  Future.from(1)
    .then(num => {
      assert.equals(num, 1);
      return num + "";
    })
    .then(str => {
      assert.equals(str, "1");
      return Future.from(str + ":result");
    })
    .then(result => {
      assert.equals(result, "1:result");
      assert.end();
    });
});

tape("Future cancel", (assert: tape.Test) => {
  let called = false;

  new Future(resolve => process.nextTick(() => resolve(1)))
    .cancel()
    .then(() => {
      called = true;
    });

  setTimeout(() => {
    assert.equals(called, false);
    assert.end();
  }, 1);
});

tape("Future resume", (assert: tape.Test) => {
  let called = false;

  new Future(resolve => process.nextTick(() => resolve(1)))
    .cancel()
    .then(() => {
      called = true;
    })
    .resume();

  setTimeout(() => {
    assert.equals(called, true);
    assert.end();
  }, 1);
});

tape("Future all", (assert: tape.Test) => {
  Future.all<number>(
    new Future(resolve => setTimeout(() => resolve(1), 1)),
    new Future(resolve => process.nextTick(() => resolve(2))),
    Future.from(3)
  ).then(results => {
    assert.deepEqual(results, [1, 2, 3]);
    assert.end();
  });
});

tape("Future race", (assert: tape.Test) => {
  Future.race<number>(
    new Future(resolve => setTimeout(() => resolve(1), 1)),
    new Future(resolve => process.nextTick(() => resolve(2))),
    Future.from(3)
  ).then(value => {
    assert.deepEqual(value, 3);
    assert.end();
  });
});

tape("Future async", async (assert: tape.Test) => {
  const result = await Future.from(1);
  assert.deepEqual(result, 1);
  assert.end();
});
