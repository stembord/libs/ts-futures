import { err, ok, Result } from "@stembord/core/src/result";
import { METHODS } from "http";
import * as request from "request";
import { isFunction } from "util";
import { Future } from "../Future";

export type RequestResult = Result<request.Response, request.Response | Error>;

export type IMethod = (
  uri: string,
  options?: request.Options
) => Future<RequestResult>;

export interface IMethods {
  [key: string]: IMethod;
}

export const methods = METHODS.reduce(
  (acc, key) => {
    const method = key.toLowerCase(),
      fn = (request as any)[method];

    if (isFunction(fn)) {
      acc[method] = (uri: string, options?: request.Options) =>
        new Future(resolve => {
          fn(uri, options, (error: Error, response: request.Response) => {
            if (error) {
              resolve(err(error));
            } else if (response.statusCode < 400) {
              resolve(ok(response));
            } else {
              resolve(err(response));
            }
          });
        }) as any;
    }

    return acc;
  },
  {} as IMethods
);
