import * as tape from "tape";
import { methods } from "../lib";

tape("Request", (assert: tape.Test) => {
  methods.get("https://google.com").then(result => {
    result
      .map(response => {
        assert.equals(response.statusCode, 200);
        assert.end();
      })
      .mapErr(error => {
        assert.end(error);
      });
  });
});
